#!/usr/bin/python

from random import randint
from time import time

def imprimir_matriz(matriz):
    print("")
    for fila in matriz:
        print(fila)

def multiplicacion_secuencial(A, B, C):
    N = len(A)

    for i in range(0, N):
        for j in range(0, N):
            for k in range(0, N):
                C[i][j] += A[i][k] * B[k][j]

def multiplicacion_secuencial_transpuesta(A, B, C):
    N = len(A)

    for i in range(0, N):
        for j in range(0, N):
            for k in range(0, N):
                C[i][j] += A[i][k] * B[j][k]

N = int(input('Ingrese tamano: '))

A = [ [0 for x in range(N)] for y in range(N) ]
B = [ [0 for x in range(N)] for y in range(N) ]
Bt = [ [0 for x in range(N)] for y in range(N) ]
C = [ [0 for x in range(N)] for y in range(N) ]
Ct = [ [0 for x in range(N)] for y in range(N) ]

for i in range(0, N):
    for j in range(0, N):
        A[i][j] = randint(1, 9)
        B[i][j] = randint(1, 9)

for i in range(0, N):
    for j in range(0, N):
        Bt[i][j] = B[j][i]

tiempo_inicial = time()
multiplicacion_secuencial(A, B, C)
tiempo_final = time()
print("Tiempo secuencial:", tiempo_final - tiempo_inicial)

tiempo_inicial = time()
multiplicacion_secuencial_transpuesta(A, Bt, Ct)
tiempo_final = time()
print("Tiempo secuencial transpuesto:", tiempo_final - tiempo_inicial)

# imprimir_matriz(A)
# imprimir_matriz(B)
# imprimir_matriz(Bt)
# imprimir_matriz(C)
# imprimir_matriz(Ct)
